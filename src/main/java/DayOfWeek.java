public enum DayOfWeek {
    SUNDAY("sunday"), MONDAY("monday"), TUESDAY("tuesday"), WEDNESDAY("wednesday"), THURSDAY("thursday"),FRIDAY("friday"),SATURDAY("saturday");


    private final String value;

    DayOfWeek(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
