import org.junit.Assert;
import org.junit.Test;

public class ToStringTest {

    @Test
    public void testToString()
    {
        Human human = new Human();
        String expected = "Human{name='null', surname='null', year=0, iq='0', mother='null', father='null', pet='null, schedule='null'}";
        Assert.assertEquals(expected, human.toString());
    }

}
