import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class childDeleteObjectArrayTest {


    @Test
    public void countFamilyTest()
    {
        Family.ADD_BASE_CHILD(new Human("Test", "Test", 1900));
        Family.ADD_BASE_CHILD(new Human("Test2", "Test2", 1901));
        Family.ADD_BASE_CHILD(new Human("Test3", "Test3", 1902));

        int countFamily = 3;

        int countResult = 0;
        for (int i = 1; i< Family.DataBaseChild.length; i++ ){
            if(Family.DataBaseChild[i] != null){
                countResult ++;
            }
        }
        Assert.assertEquals(countFamily, countResult+1);
    }

    @Test
    public void deleteChild()
    {
        String expected = "Ребенок удален из базы";
        Assert.assertEquals(expected, Family.deleteChild(0));
    }
    @Test
    public void ArrayChildNull()
    {
        for (int i = 0; i< Family.DataBaseChild.length; i++ ){
            Assert.assertEquals(null, Family.DataBaseChild[i]);
        }
    }
    @Test
    public void deleteChildDiapazone()
    {
        Family.DataBaseChild[999] = new Human("Test", "Test", 1900);
        int expected = Family.DataBaseChild.length;
        Assert.assertEquals(expected,Family.DataBaseChild.length);
    }

    @Test
    public void ArrayChildNoNull()
    {
        Human addChild = new Human("Test", "Test", 1900);
        Family.ADD_BASE_CHILD(addChild);
        for (int i = 0; i< Family.DataBaseChild.length; i++ ){
                if(Family.DataBaseChild[i] != null){
                    Assert.assertEquals(addChild, Family.DataBaseChild[i]);
                }
        }
    }



    @Test
    public void addChild()
    {
        Human addChild = new Human("Test", "Test", 1900);
        Family.ADD_BASE_CHILD(addChild);

        for (int i = Family.DataBaseChild.length -1; i >= 0; i-- ){
            if(Family.DataBaseChild[i] != null){
                Assert.assertNotEquals(addChild.toString(),Family.DataBaseChild[i]);
            }
        }

    }



}



